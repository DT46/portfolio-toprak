
const sunpath ="M40 20C40 31.0457 31.0457 40 20 40C8.9543 40 0 31.0457 0 20C0 8.9543 8.9543 0 20 0C31.0457 0 40 8.9543 40 20Z";

const moonpath ="M15.5 20C15.5 31.0457 20 40 20 40C8.9543 40 0 31.0457 0 20C0 8.9543 8.9543 0 20 0C20 0 15.5 8.9543 15.5 20Z";

const nightmode = document.querySelector("#nightmode");

let toggle= false;

nightmode.addEventListener("click", ()=>{
    const timeline = anime.timeline({
        duration: 500,
        easing: "easeOutQuad",
    
    });
    timeline
    .add({
        targets:".sun",
        d: [{value: toggle ? sunpath : moonpath}]
    })
    .add({
        targets: '#nightmode',
        rotate: 180,
    })
    .add({
        targets: "section",
        color: toggle ? "#1B1B1B" : "#D3D3D3",
        backgroundColor: toggle ? "#D3D3D3" : "#1B1B1B"
    })
    .add({
        targets: "#mode",
        color: toggle ? "#1B1B1B" : "#D3D3D3",
        duration: 100
    });

    if(!toggle){
        toggle=true;
    }
    else{
        toggle=false;
    } 
    });

$('.project')[0].scrollIntoView({
    behavior: 'smooth'
});

var elements = document.querySelectorAll('#positions .el');

anime({
  targets: elements,
  translateX: 270
});

$('.photos').slick({
    slidesToShow: 3,
    slidesToScroll: 1,
    autoplay: true,
    autoplaySpeed: 2000,
  });